require 'rails_helper'

RSpec.describe TasksController, type: :controller do
  describe "GET /index" do
    user = FactoryBot.create(:user)
    bucket = FactoryBot.create(:bucket)
    task = bucket.tasks.create(name: 'name', description: 'this is task')
    before { sign_in user }

    it 'index' do
      get :index
      expect(response).to render_template(:index)
      expect(response).to have_http_status(200)
    end

    it 'new' do
      get :new
      expect(response).to render_template(:new)
      expect(response).to have_http_status(200)
    end

    describe 'create' do
      before do
        post :create, params: { task: { name: 'name', description: 'this is task',bucket_id: bucket.id } }
      end
      it 'creates record' do
        expect {
          post :create, params: { task: { name: 'name', description: 'this is task',bucket_id: bucket.id } }
        }.to change(Task, :count).by(1)
      end

      it 'new status' do
        expect(Task.last.status).to eq "Pending"
        expect(Task.last.bucket.status).to eq "Pending"
      end

      it "redirect_to task_path" do
        expect(response).to redirect_to(task_path(Task.last))
      end
    end

    it 'show' do
      get :show ,params: { id: task.id }
      expect(response).to render_template(:show)
      expect(response).to have_http_status(200)
    end

    it 'edit' do
      get :edit ,params: { id: task.id }
      expect(response).to render_template(:edit)
      expect(response).to have_http_status(200)
    end

    describe 'update' do
      before do
        put :update ,params: { id: task.id ,task: { name: 'update_task' }}
        task.reload
      end
      it 'updates record' do
        expect(task.name).to eq 'update_task'
      end

      it 'task update_status' do
        expect(task.status).to eq 'Pending'
        expect(task.bucket.status).to eq 'Pending'
      end

      it 'redirect_to task_path' do
        expect(response).to redirect_to(task_path(task))
      end
    end

    it 'detroy' do
      new_task = user.tasks.create(name: "new_task", description: "this is task")
      expect(new_task.persisted?).to be true
      delete :destroy, params: { id: new_task.id }
      expect(Task.find_by(name: "new_task").present?).to be false
      expect(response).to redirect_to(tasks_path)
    end

    it 'sign out & sign in' do
      sign_out user
      expect(controller.current_user).to be_nil

      sign_in user
      expect(controller.current_user).to eq(user)
    end

  end
end