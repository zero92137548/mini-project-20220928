require 'rails_helper'

RSpec.describe BucketsController, type: :controller do
  describe "GET /index" do
    user = FactoryBot.create(:user)
    bucket = user.buckets.create(name: "name", description: "this is bucket")
    before { sign_in user }

    it 'index' do
      get :index
      expect(response).to render_template(:index)
      expect(response).to have_http_status(200)
    end

    it 'new' do
      get :new
      expect(response).to render_template(:new)
      expect(response).to have_http_status(200)
    end

    describe 'create' do
      before do
        post :create, params: { bucket: { name: 'name', description: 'this is bucket' } }
      end
      it 'creates record' do
        expect {
          post :create, params: { bucket: { name: 'name', description: 'this is bucket' } }
        }.to change(Bucket, :count).by(1)
      end

      it 'new bucket status' do
        expect(Bucket.last.status).to eq "Empty"
      end

      it "redirect_to bucket_path" do
        expect(response).to redirect_to(bucket_path(Bucket.last))
      end
    end

    it 'show' do
      get :show ,params: { id: bucket.id }
      expect(response).to render_template(:show)
      expect(response).to have_http_status(200)
    end

    it 'edit' do
      get :edit ,params: { id: bucket.id }
      expect(response).to render_template(:edit)
      expect(response).to have_http_status(200)
    end

    describe 'update' do
      before do
        put :update ,params: { id: bucket.id ,bucket: { name: 'update_bucket' }}
        bucket.reload
      end
      it 'updates record' do
        expect(bucket.name).to eq 'update_bucket'
      end

      it 'bucket update_status' do
        expect(bucket.status).to eq 'Empty'
      end

      it 'redirect_to bucket_path' do
        expect(response).to redirect_to(bucket_path(bucket))
      end
    end

    it 'detroy' do
      new_bucket = user.buckets.create(name: "new_bucket", description: "this is bucket")
      expect(new_bucket.persisted?).to be true
      delete :destroy, params: { id: new_bucket.id }
      expect(Bucket.find_by(name: "new_bucket").present?).to be false
      expect(response).to redirect_to(buckets_path)
    end

    it 'sign out & sign in' do
      sign_out user
      expect(controller.current_user).to be_nil

      sign_in user
      expect(controller.current_user).to eq(user)
    end
  end
end

