FactoryBot.define do
  factory :bucket do
    user
    name { 'name' }
    description { 'this is bucket' }
    status { 'Empty' }
  end
end
