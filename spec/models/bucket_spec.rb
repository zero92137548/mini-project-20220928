require 'rails_helper'

RSpec.describe Bucket, type: :model do
  user = FactoryBot.create(:user)
  bucket = FactoryBot.create(:bucket)
  it 'has_many :tasks' do
    task = bucket.tasks.create
    expect(task.bucket_id).to eq(bucket.id)
  end

  it 'belongs_to :user' do
    bucket_test = user.buckets.create
    expect(bucket_test.user_id).to eq(user.id)
  end

  it 'owner_cannot_change' do
    user2 = FactoryBot.create(:user)
    bucket.update(user_id: 2)
    expect(bucket.errors.full_messages).to include ("Owner can not be changed")
  end

  it 'get_by_status("Empty")' do
    bucket_test = user.buckets.create(status: "Empty")
    expect(Bucket.get_by_status("Empty")).to include bucket_test 
  end
  
  it 'get_by_status("Pending")' do
    bucket_test = user.buckets.create(status: "Pending")
    expect(Bucket.get_by_status("Pending")).to include bucket_test 
  end

  it 'get_by_status("Completed")' do
    bucket_test = user.buckets.create(status: "Completed")
    expect(Bucket.get_by_status("Completed")).to include bucket_test 
  end
  
  it 'update_status([])' do
    bucket.tasks.destroy_all
    bucket.update_status
    expect(bucket.status).to eq("Empty")
  end

  it 'update_status("Pending")' do
    bucket.tasks.create(status: "Pending")
    bucket.update_status
    expect(bucket.status).to eq("Pending")
  end

  it 'update_status("Completed")' do
    bucket.tasks.destroy_all
    bucket.tasks.create(status: "Completed")
    bucket.update_status
    expect(bucket.status).to eq("Completed")
  end

  it 'tasks_by_status("Pending")' do
    bucket.tasks.destroy_all
    task = bucket.tasks.create(status: "Pending")
    expect(bucket.tasks_by_status("Pending")).to include task
  end

  it 'tasks_by_status(Completed）' do
    bucket.tasks.destroy_all
    task = bucket.tasks.create(status: "Completed")
    expect(bucket.tasks_by_status("Completed")).to include task
  end

end
