require 'rails_helper'

RSpec.describe User, type: :model do
  it 'has_many :buckets' do
    user = FactoryBot.create(:user)
    bucket = user.buckets.create
    expect(user.buckets).to include bucket
  end

  it 'has_many :buckets count' do
    user = FactoryBot.create(:user)
    5.times { user.buckets.create }
    expect(user.buckets.count).to be 5
  end
end
