require 'rails_helper'

RSpec.describe Task, type: :model do
  it 'belongs_to :bucket' do
    bucket = FactoryBot.create(:bucket)
    task = bucket.tasks.create
    expect(task.bucket_id).to eq(bucket.id)
  end

  it 'self.get_by_status("Pending")' do
    bucket = FactoryBot.create(:bucket)
    task = bucket.tasks.create(status: "Pending")
    expect(Task.get_by_status("Pending")).to include task
  end
  it 'self.get_by_status("Completed")' do
    bucket = FactoryBot.create(:bucket)
    task = bucket.tasks.create(status: "Completed")
    expect(Task.get_by_status("Completed")).to include task
  end
end
