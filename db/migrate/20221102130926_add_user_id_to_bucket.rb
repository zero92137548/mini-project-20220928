class AddUserIdToBucket < ActiveRecord::Migration[6.1]
  def change
      add_reference :buckets, :user
  end
end
